<?php

/*
 * any_taxonomy_filter_handler extends the views_handler_
 */

class any_taxonomy_filter_handler extends views_handler_filter {

  var $taxonomy_terms = NULL;

  function construct() {
    parent::construct();
    $this->taxonomy_terms = array();
  }

  function option_definition(){
    $options = parent::option_definition();

    $options["operator"]["default"] = "any";

    $options["use_operator"] = array(
      "default"=>TRUE,
      "bool"=>TRUE,
    );

    return $options;
  }

  function operators() {
    $operators = array(
      'any' => array(
        'title' => t('Matches any search term'),
        'short' => t('any'),
        'short_single' => t('||'),
        'method' => 'no_op',
        'values' => 1,
      ),
      'all' => array(
        'title' => t('Matches all search terms'),
        'short' => t('all'),
        'short_single' => t('&&'),
        'method' => 'no_op',
        'values' => 1,
      ),
    );

    return $operators;
  }

  function no_op() {
    //Don't need to do anything; operators are for the pre_query.
  }

  function operator_options($which = "title") {
    $options = array();

    foreach ($this->operators() as $id=>$info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  function value_form(&$form, &$form_state) {
    $form["value"] = array(
      "#type"=>"textfield",
      "#title"=>"Value",
      "#default"=>$this->value[0]
    );
  }

  function get_values() {
    //Remove empty values from the array before returning it
    return array_filter(explode(' ', strtolower($this->value[0])));
  }

  function post_execute(&$values) {
    //We only need to do this post processing if the operator is "all"

    if (strlen($this->value[0]) == 0 || $this->operator !== "all") {
      return;
    }
    //drupal_set_message(count($values));
    //$values is a bunch of nodes... sort of.
    //Use this array to cache taxonomy fields per bundle type.
    $bundle_taxonomy_fields = array();
    foreach ($values as $ind=>$node) {
      $node = $node->_field_data["nid"]["entity"];
      $ids = entity_extract_ids("node", $node);
      $fields = NULL;
      //For each node, we have to check its type, then grab any taxonomy field values off of it
      if (isset($bundle_taxonomy_fields[$ids[2]])) {
        $fields = $bundle_taxonomy_fields[$ids[2]];
      } else {

        //Extract taxonomy field names from the bundle
        $fields = array();
        foreach (array_keys(field_info_instances('node', $ids[2])) as $field_name) {
          $field = field_info_field($field_name);
          $type = $field["type"];
          if ($type == "taxonomy_term_reference") {
            $fields[] = $field_name;
          }
        }

        $bundle_taxonomy_fields[$ids[2]] = $fields;
      }

      $tids = array();
      foreach ($fields as $field_name) {
        $field_values = field_get_items("node", $node, $field_name);
        if ($field_values) {
          foreach ($field_values as $field_value){
            $tids[] = $field_value["tid"];
          }
        }
      }


      $search_terms = $this->get_values();
      $matched = array();
      foreach ($search_terms as $search_term) {
        //We're looking for matches for each term, so we can check in
        //$this->taxonomy_terms[$search_term] to find matching TIDs
        $matching_tids = $this->taxonomy_terms[$search_term];

        //all we need is the intersection of the two sets to be nonempty.
        if (array_intersect($matching_tids, $tids)) {
          $matched[$search_term] = TRUE;
        }
      }

      if (count($matched) < count($search_terms)) {
        //Remove this node from the value set.
        unset($values[$ind]);
      }
    }
  }

  function query() {
    //parent::query();
    //Join the taxonomy_index table, checking for tids in the matched terms

    //$this->taxonomy_terms = array(106, 647);
    $terms = FALSE;

    if (strlen($this->value[0])) {

      if ($this->operator == "any") {
        $any = TRUE;
      } else {
        $any = FALSE;
      }

      $values = $this->get_values();
      $terms = db_query("SELECT tid, LOWER(name) as name FROM taxonomy_term_data");
      $terms = $terms->fetchAllAssoc("tid");
      $matched = array();
      foreach ($values as $value) {
        //For the first run through the array, we need to actually grab all matching data, regardless of operator
        foreach ($terms as $tid=>$term) {
          if (strpos($term->name, $value) !== FALSE) {
            //Save for the post_execution function, to filter the matches.
            $this->taxonomy_terms[$value][] = $tid;
            $matched[$tid] = TRUE;
          }
        }
      }

      $terms = array_keys($matched);

    }

    if ($terms !== FALSE) {
      $join = new views_join();

      $join->table = "taxonomy_index";
      $join->left_table = "node";

      $join->field = "nid";
      $join->left_field = "nid";

      $join->type = "inner";
      if ($terms) {
        $join->extra = "tid in (". implode(', ', $terms) . ")";
      } else {
        //Need to not have any results, so a blatant falsism should
        $join->extra = "TRUE = FALSE";
      }
      $tax_table = $this->query->ensure_table("taxonomy_index", "node", $join);
    }
  }
}
