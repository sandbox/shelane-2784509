<?php


//Implements hook_views_data_alter
//Lets views know about any new handlers we provide

function any_taxonomy_filter_views_data_alter(&$data) {
  $data["node"]["any_taxonomy_filter_handler"] = array(
    'title'=> t("Any Taxonomy Match"),
    "help"=> t("Node has any taxonomy that matches the provided string."),
    "filter"=>array(
      "handler"=> "any_taxonomy_filter_handler",
    )
  );
}
